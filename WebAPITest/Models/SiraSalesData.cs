﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebAPITest.Models
{
    public class SiraSalesData
    {
        //新規SIRAデータ登録
        public static void RegisterNewSiraSalesData(string skkcode, string data)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string dtstr = dt.ToString("yyyyMMddHHmm");
            try
            {
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO  SIRASalesData (ID,SKKコード,日付,データ) VALUES ('" + count + "','" + skkcode + "','" + dtstr + "','" + data + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}