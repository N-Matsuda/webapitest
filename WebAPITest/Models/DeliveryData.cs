﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebAPITest.Models
{
    //デバイスより送信される荷下ろしデータを登録、管理するクラス
    public class DeliveryData
    {
        private DataTable DeliveryDataTableDT;
        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKCode
        private const int CurrentDateNo = 2;   //最新登録日
        private const int DeliveryDataColNo = 3;

        public DeliveryData()
        {
            DataTableCtrl.InitializeTable(DeliveryDataTableDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "SELECT * FROM DeliveryData WHERE SKKコード= '" + skkcode + "' ORDER BY 日付";
                }
                else
                {
                    sqlstr = "SELECT * FROM DeliveryData WHERE SKKコード LIKE '" + skkcode + "%' ORDER BY 日付";
                }
                DataTableCtrl.InitializeTable(DeliveryDataTableDT);
                DeliveryDataTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, DeliveryDataTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return DeliveryDataTableDT.Rows.Count;
        }

        //Deliveryレコード取り出し
        public string GetDeliveryData(int lineno)
        {
            try
            {
                if ((DeliveryDataTableDT != null) && (lineno < DeliveryDataTableDT.Rows.Count))
                {
                    return DeliveryDataTableDT.Rows[lineno][DeliveryDataColNo].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //DBテーブル削除 SKKコード
        public void DeleteTableSkkcode(string skkcode)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "DELETE FROM DeliveryData WHERE SKKコード= '" + skkcode + "'";
                }
                else
                {
                    sqlstr = "DELETE FROM DeliveryData WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //新規荷下ろしデータ登録
        public void RegisterNewDeliveryData(string skkcode, string data)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string dtstr = dt.ToString("yyyyMMddHHmm");
            try
            {
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO  DeliveryData (ID,SKKコード,日付,データ) VALUES ('" + count + "','" + skkcode + "','" + dtstr + "','" + data + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

    }
}