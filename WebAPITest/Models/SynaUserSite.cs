﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebAPITest.Models
{
    //シナネンの各ユーザーごとに表示可能なSKKコードを管理するクラス
    public class SynaUserSite
    {
        private DataTable SynaUserSiteDT;

        //コンストラクター
        public SynaUserSite()
        {
            DataTableCtrl.InitializeTable(SynaUserSiteDT);
        }
        //新規ユーザー登録
        public void CreateNewuser(string username)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                string sqlstr;
                con.ConnectionString = GlobalVar.DBCONNECTION;
                con.Open();
                cmd.CommandText = "SELECT ユーザー名 FROM SynaUserSite WHERE ユーザー名='" + username + "'";
                cmd.Connection = con;

                // SQLを実行します。
                SqlDataReader reader = cmd.ExecuteReader();

                string uname = "";
                // 結果を表示します。
                while (reader.Read())
                {
                    uname = (string)reader.GetValue(0);
                }
                reader.Close();
                if (uname == "")
                {
                    sqlstr = "INSERT INTO SynaUserSite (ユーザー名,サイトグループ1,サイトグループ3,サイトグループ3) VALUES ('" + username + "','','','')";
                    cmd = new SqlCommand(sqlstr, con);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //グループテーブル読み込み
        public List<string> OpenTable(string username)
        {
            List<string> stringList = new List<string>();
            try
            {
                string sqlstr = "SELECT ユーザー名,サイトグループ1 FROM SynaUserSite WHERE ユーザー名  ='" + username + "'";
                DataTableCtrl.InitializeTable(SynaUserSiteDT);
                SynaUserSiteDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynaUserSiteDT);

                string unamestr = SynaUserSiteDT.Rows[0][1].ToString();
                string[] chkdskkcode = new string[0];
                if (unamestr != "")
                {
                    chkdskkcode = unamestr.Split(',');
                }
                stringList.AddRange(chkdskkcode);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return stringList;
        }
        //指定ユーザーに対して登録可能なSKKコード一覧を指定し、登録する。
        public bool RegTable(List<string> chksites, string username)
        {
            bool ret = true;
            try
            {
                string sitelst = "";
                bool firstss = true;
                foreach (string siten in chksites)
                {
                    if (firstss == true)
                    {
                        sitelst += siten;
                        firstss = false;
                    }
                    else
                    {
                        sitelst += "," + siten;
                    }
                }
                string sqlstr = "UPDATE SynaUserSite SET サイトグループ1 = '" + sitelst + "' WHERE ユーザー名 ='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

    }
}