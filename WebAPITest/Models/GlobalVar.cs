﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebAPITest.Models
{
    public class GlobalVar
    {
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";
        public static string GetAP8Data = "GetAP8Data";
        public static string RegetAP8Data = "RegetAP8Dt";
        public static string PostCardLockData = "PostCLData";
        //public static string ServerPath { get; set; }
        public static int NUMTANK = 20;
    }

    //AP-8集信タイプ
    public enum Ap8ComType
    {
        GetAP8Dat = 0, //AP-8集信
        GetAP8DatRetry = 1, //AP-8再集信
        PutCardLockDat = 2  //カードロックデータ配信
    }

    public class DataTableCtrl
    {
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }
    public class DBCtrl
    {
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.ExecuteNonQuery();
            cn.Close();
        }
    }

}