﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebAPITest.Models
{
    //デバイスから送信されるSIRAデータを登録、管理するクラス
    public class SiraData
    {
        private DataTable SIRADataTableDT;

        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKCode
        private const int CurrentDateNo = 2;   //最新登録日
        private const int SIRADataColNo = 3;

        //コンストラクタ
        public SiraData()
        {
            DataTableCtrl.InitializeTable(SIRADataTableDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "SELECT * FROM SIRAData WHERE SKKコード= '" + skkcode + "' ORDER BY 日付";
                }
                else
                {
                    sqlstr = "SELECT * FROM SIRAData WHERE SKKコード LIKE '" + skkcode + "%' ORDER BY 日付";
                }
                DataTableCtrl.InitializeTable(SIRADataTableDT);
                SIRADataTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRADataTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SIRADataTableDT.Rows.Count;
        }

        //SIRAレコード取り出し
        public string GetSIRAData(int lineno)
        {
            try
            {
                if ((SIRADataTableDT != null) && (lineno < SIRADataTableDT.Rows.Count))
                {
                    return SIRADataTableDT.Rows[lineno][SIRADataColNo].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //DBテーブル削除 SKKコード
        public void DeleteTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "DELETE FROM SIRAData WHERE SKKコード= '" + skkcode + "'";
                }
                else
                {
                    sqlstr = "DELETE FROM SIRAData WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //新規SIRAデータ登録
        public void RegisterNewSiraData(string skkcode, string data)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string dtstr = dt.ToString("yyyyMMddHHmm");
            try
            {
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO  SIRAData (ID,SKKコード,日付,データ) VALUES ('" + count + "','" + skkcode + "','" + dtstr + "','" + data + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }



    }
}