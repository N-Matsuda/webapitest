﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebAPITest.Models
{
    //シナネンAP-8のあるSSを管理するクラス
    public class SynaSiteDatAP8
    {
        private DataTable SynSiteDatAP8DT;

        //constructor
        public SynaSiteDatAP8()
        {
            DataTableCtrl.InitializeTable(SynSiteDatAP8DT);
        }

        //テーブル読み込み
        public void OpenTable()
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaSiteDatAP8";
                DataTableCtrl.InitializeTable(SynSiteDatAP8DT);
                SynSiteDatAP8DT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteDatAP8DT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SynSiteDatAP8DT.Rows.Count;
        }

    }
}