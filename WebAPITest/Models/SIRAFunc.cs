﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Diagnostics;

namespace WebAPITest.Models
{
    public class SIRAFunc
    {
        private DataTable SIRAFuncTableDT;
        private const int IdColNo = 0;
        private const int SiteCodeColNo = 1;    //SiteCode
        private const int MakePdfColNo = 2;     //PDF作成
        private const int TrfLOBColNo = 3;     //LOB社転送
                                               //コンストラクター
        public SIRAFunc()
        {
            DataTableCtrl.InitializeTable(SIRAFuncTableDT);
        }

        //テーブル読み込み
        public void OpenTable(string skkcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM SIRAFunc WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SIRAFuncTableDT);
                SIRAFuncTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRAFuncTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //true -- PDFファイル作成転送 
        public bool GetPDFCreate()
        {
            bool bpdfcr = false;
            if (SIRAFuncTableDT != null)
            {
                try
                {
                    int ipdfcr = (int)SIRAFuncTableDT.Rows[0][MakePdfColNo];
                    bpdfcr = (ipdfcr > 0) ? true : false;

                }
                catch
                {
                    return bpdfcr;
                }
            }
            return bpdfcr;
        }

        //true -- LOBへデータ転送 
        public bool GetLOBTrf()
        {
            bool blobtrf = false;
            if (SIRAFuncTableDT != null)
            {
                try
                {
                    int ilobtrf = (int)SIRAFuncTableDT.Rows[0][TrfLOBColNo];
                    blobtrf = (ilobtrf > 0) ? true : false;

                }
                catch
                {
                    return blobtrf;
                }
            }
            return blobtrf;
        }
    }
}