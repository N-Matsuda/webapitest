﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Configuration;

namespace WebAPITest.Models
{
    //シナネン各SSのAP8の通信ログを管理するクラス
    public class SynaSiteAP8Log
    {
        private DataTable SynSiteAP8LogDT;
        private DataTable SynSiteCLLogDT;       //AP-8カードロックログデータテーブル
        private const int AP8COMLEN = 10;       //AP-8コマンド
        private const int AP8STATLEN = 3;       //AP-8コマンド実行ステータス
        private const int AP8DATAMAXLEN = 2000; //AP-8集信データ

        //カードロック配信　SKKコード、時間、ログ、施設名
        private const int CLSKKCODECOLNO = 0;   //SKKコード
        private const int CLTIMECOLNO = 1;      //時間
        private const int CLLOGCOLNO = 2;       //ログ
        private const int CLSITENAMECOLNO = 3;  //施設名

        //constructor
        public SynaSiteAP8Log()
        {
            DataTableCtrl.InitializeTable(SynSiteAP8LogDT);
        }

        //テーブル読み込み
        public void OpenTable()
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaSiteAP8Log";
                DataTableCtrl.InitializeTable(SynSiteAP8LogDT);
                SynSiteAP8LogDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteAP8LogDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SynSiteAP8LogDT.Rows.Count;
        }
        //集信ログレコードを追加
        public void InsertRecord(string value)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                string dtstr = dt.ToString("yyyyMMddHHmm");

                string skkcode = value.Substring(0, 10);        //SKKコード取り出し
                value = value.Substring(10);
                string opcode = value.Substring(0, AP8COMLEN);  //AP-8コマンド取り出し
                int opnum = 0;
                if (opcode == GlobalVar.RegetAP8Data)
                    opnum = 1;
                else if (opcode == GlobalVar.PostCardLockData)
                    opnum = 2;
                value = value.Substring(AP8COMLEN);
                string statcode = value.Substring(0, AP8STATLEN);   //AP-8コマンド実行ステータス取り出し
                value = value.Substring(AP8STATLEN);
                string ap8dat = value;                              //AP-8集信データ
                if (ap8dat.Length > AP8DATAMAXLEN)
                    ap8dat = ap8dat.Substring(0, AP8DATAMAXLEN);

                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO SynaSiteAP8Log (ID, SKKコード, 時間, 集信タイプ, ログ, データ ) VALUES ('";
                sqlstr += count + "','" + skkcode + "','" + dtstr + "','" + opnum + "','" + statcode + "','" + ap8dat + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("在庫履歴書き込み失敗");
            }
        }

        //テーブル読み込み(カードロック配信結果) SKKコード、時間、ログ、施設名
        public void OpenDeliveryTable()
        {
            try
            {
                string sqlstr = "SELECT SynaSiteAP8Log.SKKコード, SynaSiteAP8Log.時間, SynaSiteAP8Log.ログ, SynaSiteDat.SS名 FROM SynaSiteAP8Log INNER JOIN SynaSiteDat ON SynaSiteAP8Log.SKKコード = SynaSiteDat.SKKコード WHERE SynaSiteAP8Log.集信タイプ ='" + Ap8ComType.PutCardLockDat + "'";
                DataTableCtrl.InitializeTable(SynSiteCLLogDT);
                SynSiteCLLogDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteCLLogDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得(配信）
        public int GetNumOfDeliveryRecord()
        {
            try
            {
                return SynSiteCLLogDT.Rows.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }
        //配信SKKコード取得
        public string GetCLSkkcode(int lineno)
        {
            string skkcode = "";
            try
            {
                if ((SynSiteCLLogDT != null) && (lineno < SynSiteCLLogDT.Rows.Count))
                {
                    skkcode = SynSiteCLLogDT.Rows[lineno][CLSKKCODECOLNO].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return skkcode;
        }

        //配信時間取得
        public string GetCLLogTime(int lineno)
        {
            string cltime = "";
            try
            {
                if ((SynSiteCLLogDT != null) && (lineno < SynSiteCLLogDT.Rows.Count))
                {
                    cltime = SynSiteCLLogDT.Rows[lineno][CLTIMECOLNO].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return cltime;
        }

        //配信ログ取得
        public string GetCLLog(int lineno)
        {
            string cllog = "";
            try
            {
                if ((SynSiteCLLogDT != null) && (lineno < SynSiteCLLogDT.Rows.Count))
                {
                    cllog = SynSiteCLLogDT.Rows[lineno][CLLOGCOLNO].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return cllog;
        }

        //配信施設名取得
        public string GetCLSitename(int lineno)
        {
            string sitename = "";
            try
            {
                if ((SynSiteCLLogDT != null) && (lineno < SynSiteCLLogDT.Rows.Count))
                {
                    sitename = SynSiteCLLogDT.Rows[lineno][CLSITENAMECOLNO].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return sitename;
        }

    }
}