﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebAPITest.Models
{
    //モバイルルーターのデータを管理するクラスです。
    public class MRouterTable
    {
        private DataTable MobileRouterTableDT;
        private const int IdColNo = 0;
        private const int CNameColNo = 1;       //会社名
        private const int SKKCodeColNo = 2;     //SKKコード
        private const int IpAdrColNo = 3;       //IPアドレステーブル
        private const int PortNoCol = 4;      //ポート番号
        private const int NumUnfound = 5;       //ファイル連続未検出回数
        private const int DataSw = 6;           //在庫データ書き込みSW
        private const int DataBuf1 = 7;         //在庫データ書き込みBuffer1
        private const int DataBuf2 = 8;         //在庫データ書き込みBuffer1
        private const int UsageColNo = 9;       //用途
        private const int SiteNameColNo = 8;         //施設名
        private string cmpnyname;
        //private const string DBCONNECTION = "Data Source = mssql3.winserver.ne.jp;Initial Catalog = skk-atgs2db;User ID=skk-atgs2;Password = skkkaimatsu";

        //コンストラクター
        public MRouterTable()
        {
            DataTableCtrl.InitializeTable(MobileRouterTableDT);
        }

        //テーブル読み込み 会社名
        public void OpenTable(string cmpny)
        {
            try
            {
                //string dbpath;
                cmpnyname = cmpny;
                string sqlstr;
                if (cmpny == "*")
                    sqlstr = "SELECT * FROM MobileRouterTable";
                else
                    sqlstr = "SELECT * FROM MobileRouterTable WHERE 会社名= (N'" + cmpny + "')";
                DataTableCtrl.InitializeTable(MobileRouterTableDT);
                MobileRouterTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, MobileRouterTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    //if( skkcode.StartsWith("M1")==true )
                    //    sqlstr = "SELECT * FROM MobileRouterTable WHERE SKKコード LIKE '" + skkcode.Substring(0,9) + "%'";
                    //else
                    sqlstr = "SELECT * FROM MobileRouterTable WHERE SKKコード= '" + skkcode.Substring(0,10) + "'";
                }
                else
                {
                    sqlstr = "SELECT * FROM MobileRouterTable WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                DataTableCtrl.InitializeTable(MobileRouterTableDT);
                MobileRouterTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, MobileRouterTableDT);
                //for (int i = MobileRouterTableDT.Rows.Count-1; i >= 0; i--)
                //{
                //    string idcode = MobileRouterTableDT.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                //    if (idcode != skkcode)
                //        MobileRouterTableDT.Rows.Remove(MobileRouterTableDT.Rows[i]);
                //    if (i == 0)
                //        break;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //レコード数取得
        public int GetNumOfRecord()
        {
            return MobileRouterTableDT.Rows.Count;
        }

        //IPアドレス取得
        public string GetIPAdr(int lineno)
        {
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    return MobileRouterTableDT.Rows[lineno][IpAdrColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //ポート番号取得
        public int GetPortNo(int lineno)
        {
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    string portnum = MobileRouterTableDT.Rows[lineno][PortNoCol].ToString();
                    return int.Parse(portnum);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }

        //ファイル連続未検出回数取得
        public int GetNumUnfound(int lineno)
        {
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    return (int)MobileRouterTableDT.Rows[lineno][NumUnfound];
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }

        //ファイル連続未検出回数設定
        public void SetNumUnfound(int lineno, int unfoundno)
        {
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    MobileRouterTableDT.Rows[lineno][NumUnfound] = unfoundno;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //在庫データ書き込み
        public void SetZaikoBuffer(int lineno, string ztext)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    if (MobileRouterTableDT.Rows[lineno][DataSw] == null)
                    {
                        MobileRouterTableDT.Rows[lineno][DataSw] = 0;
                    }
                    if ((int)MobileRouterTableDT.Rows[lineno][DataSw] == 0)
                    {
                        MobileRouterTableDT.Rows[lineno][DataBuf1] = ztext;
                        MobileRouterTableDT.Rows[lineno][DataSw] = 1;
                    }
                    else
                    {
                        MobileRouterTableDT.Rows[lineno][DataBuf2] = ztext;
                        MobileRouterTableDT.Rows[lineno][DataSw] = 0;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //在庫データ前取得
        public string GetZaikoBeforeBuf(int lineno)
        {
            string zstr = "";
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    if ((int)MobileRouterTableDT.Rows[lineno][DataSw] == 0)
                    {
                        zstr = (string)MobileRouterTableDT.Rows[lineno][DataBuf1];
                    }
                    else
                    {
                        zstr = (string)MobileRouterTableDT.Rows[lineno][DataBuf2];
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }
        //在庫データ後取得
        public string GetZaikoAfterBuf(int lineno)
        {
            string zstr = "";
            try
            {
                if ((MobileRouterTableDT != null) && (lineno < MobileRouterTableDT.Rows.Count))
                {
                    if ((int)MobileRouterTableDT.Rows[lineno][DataSw] == 0)
                    {
                        zstr = (string)MobileRouterTableDT.Rows[lineno][DataBuf2];
                    }
                    else
                    {
                        zstr = (string)MobileRouterTableDT.Rows[lineno][DataBuf1];
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }
        //最新在庫データ取得
        public string GetZaikoString()
        {
            string zstr = "";
            try
            {
                if (MobileRouterTableDT != null)
                {
                    if ((int)MobileRouterTableDT.Rows[0][DataSw] == 0)
                    {
                        zstr = (string)MobileRouterTableDT.Rows[0][DataBuf2];
                    }
                    else
                    {
                        zstr = (string)MobileRouterTableDT.Rows[0][DataBuf1];
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }


        //DBコミット
        public bool UpdateDB()
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                string skkcode;
                for (int i = 0; i < MobileRouterTableDT.Rows.Count; i++)
                {
                    skkcode = MobileRouterTableDT.Rows[i][SKKCodeColNo].ToString();
                    sqlstr = "UPDATE MobileRouterTable SET DataSw='" + MobileRouterTableDT.Rows[i][DataSw].ToString() +
                         "', 在庫データ1= '" + MobileRouterTableDT.Rows[i][DataBuf1] +
                         "', 在庫データ2= '" + MobileRouterTableDT.Rows[i][DataBuf2] + "' WHERE SKKコード= '" + skkcode + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                }
                cn.Close();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }

    }
}