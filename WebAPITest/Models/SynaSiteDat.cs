﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace WebAPITest.Models
{
    //シナネンの各SSの情報を管理するクラス
    public class SynaSiteDat
    {
        private DataTable SynSiteDatDT;

        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int CmpCodeColNo = 2;     //企業社コード
        private const int SSCodeColNo = 3;      //SSコード
        private const int SiteNameColNo = 4;   //施設名
        private const int PrefNameColNo = 5;    //県名
        private const int AddressColNo = 6;     //アドレス
        private const int PortNoColNo = 7;      //ポート番号
        private const int FtpAdrColNo = 8;      //FTPアドレス
        private const int AtgStsColNo = 9;      //液面計警報状態
        private const int LkSysColNo = 10;      //漏えい検知システム

        byte[] atgwrgsts;           //液面計警報状態
        byte[] odctsyssts;          //油漏えい検知警報状態
        private const int TANKOFST = 20;
        private const int LKSYSSNSNO = 8;     //油漏えい検知システム１台当たりのセンサー数
        private const int LKSYSSNSSTSNO = 3;  //油漏えい検知システムセンサー当たりのステータスバイト数


        //constructor
        public SynaSiteDat()
        {
            DataTableCtrl.InitializeTable(SynSiteDatDT);
        }

        //テーブル読み込み
        public void OpenTable()
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaSiteDat";
                DataTableCtrl.InitializeTable(SynSiteDatDT);
                SynSiteDatDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteDatDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
        //テーブル読み込み
        public void OpenTable(string skkcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaSiteDat WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SynSiteDatDT);
                SynSiteDatDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteDatDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return SynSiteDatDT.Rows.Count;
        }

        //SKKコード取得
        public string GetSKKCode(int lineno)
        {
            try
            {
                if ((SynSiteDatDT != null) && (lineno < SynSiteDatDT.Rows.Count))
                {
                    return SynSiteDatDT.Rows[lineno][SKKCodeColNo].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //施設名取得
        public string GetSiteName(int lineno)
        {
            try
            {
                if ((SynSiteDatDT != null) && (lineno < SynSiteDatDT.Rows.Count))
                {
                    return SynSiteDatDT.Rows[lineno][SiteNameColNo].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //指定行の 液面計警報有無　液面計警報状態　油漏えい検知警報有無　油漏えい検知警報状態を取り出す
        public void OpenLine(int lineno)
        {
            try
            {
                if ((SynSiteDatDT != null) && (lineno < SynSiteDatDT.Rows.Count))
                {
                    //atgwrgsts = Encoding.ASCII.GetBytes(WgMlSysSiteDT.Rows[lineno][COLATGWRGSTS].ToString().TrimEnd());
                    //odctsyssts = Encoding.ASCII.GetBytes(WgMlSysSiteDT.Rows[lineno][COLODCTSYSSTS].ToString().TrimEnd());
                    atgwrgsts = Encoding.ASCII.GetBytes(SynSiteDatDT.Rows[lineno][AtgStsColNo].ToString());
                    odctsyssts = Encoding.ASCII.GetBytes(SynSiteDatDT.Rows[lineno][LkSysColNo].ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        //現在行の減警報状態を取り出す
        public bool ChkLowWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の満警報状態を取り出す
        public bool ChkHighWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の水検知警報状態を取り出す
        public bool ChkWtrWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のセンサー異常警報状態を取り出す
        public bool ChkSnsWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 3] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のオイルリーク警報状態を取り出す
        public bool ChkLkWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 4] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の測定下限警報状態を取り出す
        public bool ChkLowLimWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 5] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-1限警報状態を取り出す
        public bool ChkLC1WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 6] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-4限警報状態を取り出す
        public bool ChkLC4WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 7] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-5限警報状態を取り出す
        public bool ChkLC5WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 8] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-7限警報状態を取り出す
        public bool ChkLC7WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 9] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-8限警報状態を取り出す
        public bool ChkLC8WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 10] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムのセンサー通信エラー状態を取り出す
        public bool ChkLkSysComErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの水検知エラー状態を取り出す
        public bool ChkLkSysWtrErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの油検知エラー状態を取り出す
        public bool ChkLkSysOilErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の減警報状態を変更する
        public void ChgLowWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の満警報状態を変更する
        public void ChgHighWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 1] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 1] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の水検知警報状態を変更する
        public void ChgWtrWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 2] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 2] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のセンサー異常警報状態を変更する
        public void ChgSnsWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 3] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 3] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のオイルリーク警報状態を変更する
        public void ChgLkWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 4] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 4] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の測定下限警報状態を変更する
        public void ChgLowLimWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 5] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 5] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-1限警報状態を変更する
        public void ChgLC1WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 6] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 6] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-4限警報状態を変更する
        public void ChgLC4WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 7] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 7] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-5限警報状態を変更する
        public void ChgLC5WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 8] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 8] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-7限警報状態を変更する
        public void ChgLC7WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 9] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 9] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-8限警報状態を変更する
        public void ChgLC8WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 10] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 10] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムのセンサー通信エラー状態を変更する
        public void ChgLkSysComErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの水検知エラー状態を変更する
        public void ChgLkSysWtrErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの油検知エラー状態を変更する
        public void ChgLkSysOilErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //警報ステータスのアップデート
        public bool UpdateATGWrgSts(string skkcode)
        {
            bool bret = true;
            try
            {
                string wrgstr = System.Text.Encoding.ASCII.GetString(atgwrgsts);
                string odstr = System.Text.Encoding.ASCII.GetString(odctsyssts);
                string cnstr = GlobalVar.DBCONNECTION;
                string sqlstr = "UPDATE SynaSiteDat SET 液面計警報状態= '" + wrgstr + "', 油漏えい検知警報状態= '" + odstr + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }


    }
}