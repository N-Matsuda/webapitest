﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;

namespace WebAPITest.Models
{
    //SIRAデータをLOBのサーバーへ転送するためのクラスです
    public class SiraDataTrf
    {
        //Constructor
        private const int MAXTANKNO = 20;       //最大タンク数
        private string kurofilename;　//黒本データのファイル名
        //private string sircsvname;
        private string sirfilename;  //LOBに転送するファイル名
        private string cmpcode;      //会社コード
        private string sitecode;     //施設コード
        private string sitename;     //施設名
        private string skkcode;      //SKKコード
        //private bool hdsira;         //true == 高精度SIRAデータ
        SiraCompany sircmp;
        SiraSite sirsite;
        FilePath fp;
        int companynum;
        int sitenum;                //施設番号
        int seqnum;                 //施設連続番号
        const int RECOFFSET = 11;
        //通常SIRAデータ読み込みの定義
        const int NUMPERTANK = 42;  //タンクあたりバイト数
        const int NUMPERTANK2 = 32;  //タンクあたりバイト数
        const int INVOFFSET = 2;    //在庫オフセット
        const int DELIVOFFSET = 12; //荷卸し量オフセット
        const int SALESOFFSET = 22; //販売量オフセット
        //通常SIRAデータ（温度データ)読み込みの定義
        const int TMP_NUMPERTANK = 34; //タンクあたりのバイト数
        const int TMP_TNKNOOFFS = 0;   //タンク番号のオフセット
        const int TMP_TNKNOSIZE = 2;   //タンク番号のサイズ
        const int TMP_DLVSTTIMEOFFS = 2; //荷卸し開始時間オフセット (hhmm)
        const int TMP_DLVSTTIMESIZE = 4; //荷卸し開始時間サイズ
        const int TMP_DLVSTVOLOFFS = 6; //荷卸し開始時量オフセット
        const int TMP_DLVSTVOLSIZE = 6; //荷卸し開始時量サイズ
        const int TMP_DLVSTTMPOFFS = 12; //荷卸し開始時温度オフセット (XX.X℃)
        const int TMP_DLVSTTMPSIZE = 3;  //荷卸し開始時温度サイズ
        const int TMP_DLVEDTIMEOFFS = 15; //荷卸し終了時間オフセット (hhmm)
        const int TMP_DLVEDTIMESIZE = 4; //荷卸し終了時間サイズ
        const int TMP_DLVEDVOLOFFS = 19; //荷卸し終了時量オフセット
        const int TMP_DLVEDVOLSIZE = 6; //荷卸し終了時量サイズ
        const int TMP_DLVEDTMPOFFS = 25; //荷卸し終了時温度オフセット (XX.X℃)
        const int TMP_DLVEDTMPSIZE = 3;  //荷卸し終了時温度サイズ
        const int TMP_SALESVOLOFFS = 28; //販売量オフセット
        const int TMP_SALESVOLSIZE = 6;  //販売量サイズ

        //高精度SIRAデータ読み込みの定義
        //液面計データ
#if true
        const int NUMPERATGDT = 30;  //液面計データのサイズ
        const int ATGDATEOFFS = 1;   //時間のオフセット
        const int ATGDATESIZE = 10;  //時間データのサイズ
        const int ATGNUNTANK = 11;   //タンク数のオフセット
        const int ATGNNTNKSZ = 2;   //タンク数のオフセット
        const int ATGHDSZ = 13;     //液面計データヘッダーのサイズ
        const int ATGTNOOFFS = 0;   //タンク番号のオフセット
        const int ATGTNOSIZE = 2;    //タンク番号のサイズ
        const int ATGOILOFFS = 2;   //液種データのオフセット
        const int ATGOILSIZE = 2;    //液種データのサイズ
        const int ATGVOLOFFS = 4;   //液量のオフセット
        const int ATGVOLSIZE = 6;    //液量サイズ
        //const int ATGOLVLOFFS = 24;  //液位のオフセット
        //const int ATGOLVLSIZE = 6;   //液位のサイズ
        //const int ATGWLVLOFFS = 30;  //水位のオフセット
        //const int ATGWLVLSIZE = 5;   //水位のサイズ
        //const int ATGTEMPOFFS = 35;  //温度のオフセット
        //const int ATGTEMPSIZE = 3;   //温度のサイズ

#else
        const int NUMPERATGDT = 38;  //液面計データのサイズ
        const int ATGDATEOFFS = 1;   //時間のオフセット
        const int ATGDATESIZE = 12;  //時間データのサイズ
        const int ATGTNOOFFS = 13;   //タンク番号のオフセット
        const int ATGTNOSIZE = 2;    //タンク番号のサイズ
        const int ATGOILOFFS = 15;   //液種データのオフセット
        const int ATGOILSIZE = 2;    //液種データのサイズ
        const int ATGVOLOFFS = 17;   //液量のオフセット
        const int ATGVOLSIZE = 7;    //液量サイズ
        const int ATGOLVLOFFS = 24;  //液位のオフセット
        const int ATGOLVLSIZE = 6;   //液位のサイズ
        const int ATGWLVLOFFS = 30;  //水位のオフセット
        const int ATGWLVLSIZE = 5;   //水位のサイズ
        const int ATGTEMPOFFS = 35;  //温度のオフセット
        const int ATGTEMPSIZE = 3;   //温度のサイズ
#endif
        //販売データ
#if true
        const int NUMSALDT = 30;    //販売データのサイズ
        const int SALTNOOFFS = 1;   //タンク番号のオフセット
        const int SALTNOSIZE = 2;   //タンク番号のサイズ
        const int SALNZLOFFS = 3;   //ノズル番号のオフセット
        const int SALNZLSIZE = 1;   //ノズル番号のサイズ
        const int SALSTMOFFS = 4;   //販売開始時間のオフセット
        const int SALSTMSIZE = 10;  //販売開始時間のサイズ
        const int SALETMOFFS = 14;  //販売終了時間のオフセット
        const int SALETMSIZE = 10;  //販売終了時間のサイズ
        const int SALVOLOFFS = 24;  //販売量のオフセット
        const int SALVOLSIZE = 6;   //販売量のサイズ
#else
        const int NUMSALDT = 34;    //販売データのサイズ
        const int SALTNOOFFS = 1;   //タンク番号のオフセット
        const int SALTNOSIZE = 2;   //タンク番号のサイズ
        const int SALNZLOFFS = 3;   //ノズル番号のオフセット
        const int SALNZLSIZE = 1;   //ノズル番号のサイズ
        const int SALSTMOFFS = 4;   //販売開始時間のオフセット
        const int SALSTMSIZE = 12;  //販売開始時間のサイズ
        const int SALETMOFFS = 16;  //販売終了時間のオフセット
        const int SALETMSIZE = 12;  //販売終了時間のサイズ
        const int SALVOLOFFS = 28;  //販売量のオフセット
        const int SALVOLSIZE = 6;   //販売量のサイズ
#endif
        //荷卸しデータ
#if true
        const int NUMDLVDT = 30;    //荷卸しデータのサイズ
        const int DLVTNOOFFS = 1;   //タンク番号のオフセット
        const int DLVTNOSIZE = 2;   //タンク番号のサイズ
        const int DLVTMOFFS = 9;    //荷卸し時間のオフセット
        const int DLVTMSIZE = 10;   //荷卸し時間のサイズ
        const int DLVVOLOFFS = 3;  //荷卸し量のオフセット
        const int DLVVOLSIZE = 6;   //荷卸し量のサイズ
        //DateTime initmth, endmth;
#else
        const int NUMDLVDT = 21;    //荷卸しデータのサイズ
        const int DLVTNOOFFS = 1;   //タンク番号のオフセット
        const int DLVTNOSIZE = 2;   //タンク番号のサイズ
        const int DLVTMOFFS = 3;    //荷卸し時間のオフセット
        const int DLVTMSIZE = 12;   //荷卸し時間のサイズ
        const int DLVVOLOFFS = 15;  //荷卸し量のオフセット
        const int DLVVOLSIZE = 6;   //荷卸し量のサイズ
        DateTime initmth, endmth;
#endif   
        //指定されたSKKコードのSIRAデータをLOBへ転送
        public SiraDataTrf(string scode)
        {
            sirsite = new SiraSite();
            sirsite.OpenTableWSkkcode(scode);
            cmpcode = sirsite.GetCompanyCode(0); //会社コード
            sitecode = sirsite.GetSiteCode(0); //施設コード
            sitename = sirsite.GetSiteName(0);
            skkcode = scode;
            //fp = new FilePath(sircmp.GetFolderName(companynum));
            fp = new FilePath();
            seqnum = 0;
        }

        //通常SIRAデータの分析とCSVファイルの作成
        public bool AnalyzeDailySIRA(string kurodatastr)
        {
            bool bret = true;
            //KuroCsvData[] kcsvdat = new KuroCsvData[MAXTANKNO];
            DateTime dt = DateTime.Now.ToLocalTime();
            //DateTime lastday = DateTime.Now;
            string kurodatestr = "";
            try
            {
                string csvstr = "";
                string datestr;
                string tnumstr;
                string relchar; //データ信頼性
                int tanknum;
                int salesnum, delivnum, invnum, calcinv, invdif;
                int iComp;
                int recordnum = 1;
                int numpertnk = NUMPERTANK;

                var tnolist = new List<string>();
                var invlist = new List<string>();
                var saleslist = new List<string>();
                var delivlist = new List<string>();

                datestr = kurodatastr.Substring(0, 8);  //日付文字列取り出し

                string chkdatestr = datestr;
                chkdatestr = chkdatestr.Insert(6, "/").Insert(4, "/");
                dt = DateTime.Parse(chkdatestr);

                relchar = kurodatastr.Substring(8, 1);
                if ((relchar == "a") || (relchar == "@") || (relchar == "A"))
                    numpertnk = NUMPERTANK2;
                tnumstr = kurodatastr.Substring(9, 2);　//タンク数文字列取り出し
                tanknum = int.Parse(tnumstr);           //タンク数取り出し
                if ((tanknum > 0) && (tanknum <= 20))
                {
                    for (int i = 0; i < tanknum; i++)
                    {
                        //if (true == sirsite.GetTankNotLinkedOrLinkedFirst(sitenum, i + 1)) //連結でないか、連結タンクの先頭ならばレコード作成
                        if ((true == sirsite.GetTankNotLinkedOrLinkedFirst(sitenum, i + 1)) && (0 != sirsite.GetTankCapa(sitenum, i + 1))) //連結でないか、連結タンクの先頭ならばレコード作成
                        {
                            string tno;
                            tno = (i + 1).ToString();
                            tnolist.Add(tno);

                            csvstr = csvstr + "\"" + cmpcode + "\",\"" + sitecode + "\",\""; //会社コード、施設コード
                            csvstr = csvstr + sirsite.GetTankOilType(sitenum, i + 1) + "\",\""; //液種
                            csvstr = csvstr + "REC" + seqnum.ToString() + recordnum.ToString() + (i + 1).ToString() + "\",\""; //レコードID
                            csvstr = csvstr + sirsite.GetTankString(sitenum, i + 1) + "\",\""; //タンクID
                            csvstr = csvstr + datestr + "\",\""; //日付；

                            salesnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + SALESOFFSET, 7));
                            iComp = kurodatastr.Substring(RECOFFSET + numpertnk * i + SALESOFFSET + 8, 1).CompareTo("5"); //販売量四者五入
                            if (iComp >= 0)
                                salesnum++;
                            csvstr = csvstr + salesnum.ToString() + "\",\""; //販売量
                            saleslist.Add(salesnum.ToString());
                            delivnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + DELIVOFFSET, 7));
                            invnum = int.Parse(kurodatastr.Substring(RECOFFSET + numpertnk * i + INVOFFSET, 7));
                            int lastinv = sirsite.GetTankLMInv(sitenum, i + 1);

                            sirsite.SetTankLMInv(sitenum, i + 1, invnum);

                            calcinv = lastinv + delivnum - salesnum;
                            //計算在庫と実在庫の差
                            if (calcinv > invnum)
                            {
                                invdif = calcinv - invnum + 300;
                            }
                            else //calcinv <= invnum
                            {
                                invdif = invnum - invnum + 300;
                            }

                            //計算在庫と実在庫の差が大きい場合は荷卸し量を調整する
                            if (invdif > 600)
                            {
                                invdif = ((invdif + 400) / 1000) * 1000;
                                if (calcinv > invnum)
                                {
                                    if (delivnum >= invdif)
                                        delivnum = delivnum - invdif;
                                }
                                else
                                {
                                    delivnum += invdif;
                                }
                            }
                            csvstr = csvstr + delivnum.ToString() + "\",\""; //荷卸し量
                            csvstr = csvstr + invnum.ToString() + "\"\r"; //在庫量
                            delivlist.Add(delivnum.ToString());
                            invlist.Add(invnum.ToString());
                        }
                        recordnum++;
                    }
                }
                //sirsite.UpdateDBLMInv();


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return bret;
        }

    }
}