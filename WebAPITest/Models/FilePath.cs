﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WebAPITest.Models
{
    public class FilePath
    {
        private string ap8blobcontainer = "skkatg-ap8data";
        private string leveblobcontainer = "skkatg-levelog";
        private string sirablobcontainer = "skkatg-sira";

        private string accountName = "skkatgstorage";
        private string accessKey = "nNT1et0cM46CxVgHWCeDbMrEmtMffz8F50EjVGqOuwVCeDus17HMa2Anhe2JnOZJcI69rsnTlbQPc1p9y9sQlA==";
        private const int cardmaxsize = 0x40000;

        public FilePath()
        {
        }

        public string GetTodayAP8DataName(string skkcode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return skkcode + "-" + dt.ToString("yyyyMMdd") + ".csv";
        }

        public string GetSpecifiedDaysAP8DataName(string skkcode, DateTime dt)
        {
            return skkcode + "-" + dt.ToString("yyyyMMdd") + ".csv";
        }

        public string GetLatestAP8DataName(string skkcode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return skkcode + "-" + dt.ToString("yyyyMMdd") + "latest.csv";
        }

        public void WriteTodayAP8Data(string skkcode, string data)
        {
            try
            {
                string filename = GetTodayAP8DataName(skkcode);
                WriteBlob(filename, ap8blobcontainer, data, false);
                filename = GetLatestAP8DataName(skkcode);
                WriteBlob(filename, ap8blobcontainer, data, true);
            }
            catch (Exception ex)
            {
                ;
            }
        }

        public string ReadTodayAP8Data(String skkcode)
        {
            string ap8dat = "";
            try
            {
                string filename = GetTodayAP8DataName(skkcode);
                ap8dat = ReadBlob(filename, ap8blobcontainer);
            }
            catch (Exception ex)
            {
                ;
            }
            return ap8dat;
        }

        public string ReadSpecifiedDayAP8Data(string skkcode, DateTime dt)
        {
            string ap8dat = "";
            try
            {
                string filename = GetSpecifiedDaysAP8DataName(skkcode, dt);
                ap8dat = ReadBlob(filename, ap8blobcontainer);
            }
            catch (Exception ex)
            {
                ;
            }
            return ap8dat;
        }

        //blobファイルの書き込み
        private bool WriteBlob(string filename, string containername, string data, bool overwrite)
        {
            bool res = true;
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                //appendblob
                //BlockBlobではなくAppenDBlobのリファレンスを取得する
                CloudAppendBlob appendBlob = container.GetAppendBlobReference(filename);

                //書き込み
                if (overwrite == false)     //追加
                {
                    //無かったら作る
                    if (!appendBlob.Exists()) appendBlob.CreateOrReplace();
                    appendBlob.AppendText(data);
                }
                else                        //上書き
                {
                    try
                    {
                        appendBlob.Delete();
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                    appendBlob.CreateOrReplace();
                    appendBlob.AppendText(data);
                }

            }
            catch (Exception ex)
            {
                ;
            }
            return res;
        }

        //blobファイルの読み込み
        private string ReadBlob(string filename, string containername)
        {
            string ap8dat = "";
            //test 
            //filename = "Revision_History.txt";
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                //ダウンロードするファイル名を指定
                //BlockBlobではなくAppenDBlobのリファレンスを取得する
                CloudAppendBlob appendBlob = container.GetAppendBlobReference(filename);

                //無かったら作る
                if (!appendBlob.Exists())
                    return ap8dat;

                byte[] bs = new byte[cardmaxsize];
                Stream fs = appendBlob.OpenRead();
                int readSize = 0;
                for (; ; )
                {
                    //ファイルの一部を読み込む
                    int sz = fs.Read(bs, 0, bs.Length);
                    //ファイルをすべて読み込んだときは終了する
                    readSize += sz;
                    if (readSize > cardmaxsize)
                        break;
                    if (sz == 0)
                        break;

                    //部分的に読み込んだデータを使用したコードをここに記述する
                }
                //閉じる
                fs.Close();

                if(readSize < cardmaxsize)
                    bs[readSize] = 0x0;
                //ASCII エンコード
                ap8dat = System.Text.Encoding.ASCII.GetString(bs);
                if (readSize < cardmaxsize)
                {
                    ap8dat = ap8dat.Substring(0, readSize);
                }

                //データがShift-JISの場合
                //string text = System.Text.Encoding.GetEncoding("shift_jis").GetString(bs);

                //データがEUCの場合
                //string text = System.Text.Encoding.GetEncoding("euc-jp").GetString(data);

                //データがunicodeの場合
                //string text = System.Text.Encoding.Unicode.GetString(data);

            }
            catch (Exception ex)
            {
                ;
            }
            return ap8dat;
        }

        //blobファイルのダウンロード
        private bool DownloadBlob(string filename, string containername, string tofilename)
        {
            bool res = false;
            //test 
            //filename = "Revision_History.txt";
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                //ダウンロードするファイル名を指定
                CloudBlockBlob blockBlob_download = container.GetBlockBlobReference(filename);
                //無かったらエラー
                if (!blockBlob_download.Exists())
                    return res;
                //ダウンロード後のパスとファイル名を指定。
                blockBlob_download.DownloadToFile(@tofilename, System.IO.FileMode.CreateNew);

                res = true;
            }
            catch (Exception ex)
            {
                ;
            }
            return res;
        }

        //指定されたコンテナー中から古いファイルを削除
        public void DeleteOldAP8Data()
        {
            try
            {
                long fileCount = 0;
                long count = 0;

                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(ap8blobcontainer);

                //継続トークン
                BlobContinuationToken blobtoken = null;


                do
                {
                    var blobResult = blobContainer.ListBlobsSegmented(blobtoken);

                    //継続トークの取得
                    blobtoken = blobResult.ContinuationToken;

                    //取得したBlob情報を直接取得
                    var blobList = blobResult.Results.ToList();
                    string filename;

                    foreach (var item in blobList)
                    {
                        filename = item.Uri.ToString();
                        filename = System.IO.Path.GetFileName(filename);
                    }

                } while (blobtoken != null);


            }
            catch (Exception ex)
            {
                ;
            }
        }
        //指定されたコンテナー中から指定SKKコードのファイル一覧取得
        public List<string> ListAP8FilesDate(string skkcode)
        {
            List<string> ap8filedate = new List<string>();
            try
            {
                long fileCount = 0;
                long count = 0;

                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(ap8blobcontainer);

                //継続トークン
                BlobContinuationToken blobtoken = null;

                do
                {
                    var blobResult = blobContainer.ListBlobsSegmented(blobtoken);

                    //継続トークの取得
                    blobtoken = blobResult.ContinuationToken;

                    //取得したBlob情報を直接取得
                    var blobList = blobResult.Results.ToList();
                    string filename;
                    string skcode, datestr;
                    foreach (var item in blobList)
                    {
                        filename = item.Uri.ToString();
                        filename = System.IO.Path.GetFileName(filename);
                        skcode = filename.Substring(0, 10);
                        if (skcode == skkcode)
                        {
                            datestr = filename.Substring(11, 8);
                            datestr = datestr.Insert(8, "日");
                            datestr = datestr.Insert(6, "月");
                            datestr = datestr.Insert(4, "年");
                            ap8filedate.Add(datestr);
                        }
                    }

                } while (blobtoken != null);


            }
            catch (Exception ex)
            {
                ;
            }
            return ap8filedate;
        }

        //カードロックファイルの名前指定
        private string GetCardLockName()
        {
            string clname = "CardLockFile.dat";
            return clname;
        }

        //指定された文字列をカードロックデータとしてブロブ(コンテナ:"skkatg-ap8data")に書き込む。
        public bool UploadCardLockFile(string cldata)
        {
            bool res = WriteBlob(GetCardLockName(), ap8blobcontainer, cldata, true); //Cardlockファイルのブロブへの書き込み
            return res;
        }

        //カードロックファイルを読み出し文字列として返す
        public string DownloadCardLockFile()
        {
            string cldata = "";
            cldata += ReadBlob(GetCardLockName(), ap8blobcontainer);
            return cldata;
        }

    }
}