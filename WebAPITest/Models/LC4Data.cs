﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebAPITest.Models
{
    //デバイスより送信されるLC4結果データを登録、管理するクラス
    public class LC4Data
    {
        private DataTable LC4DataTableDT;

        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKCode
        private const int CurrentDateNo = 2;   //最新登録日
        private const int LC4DataColNo = 3;

        public LC4Data()
        {
            DataTableCtrl.InitializeTable(LC4DataTableDT);
        }

        //テーブル読み込み SKKコード
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "SELECT * FROM LC4Data WHERE SKKコード= '" + skkcode + "' ORDER BY 日付";
                }
                else
                {
                    sqlstr = "SELECT * FROM LC4Data WHERE SKKコード LIKE '" + skkcode + "%' ORDER BY 日付";
                }
                DataTableCtrl.InitializeTable(LC4DataTableDT);
                LC4DataTableDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, LC4DataTableDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return LC4DataTableDT.Rows.Count;
        }

        //LC4レコード取り出し
        public string GetLC4Data(int lineno)
        {
            try
            {
                if ((LC4DataTableDT != null) && (lineno < LC4DataTableDT.Rows.Count))
                {
                    return LC4DataTableDT.Rows[lineno][LC4DataColNo].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //DBテーブル削除 SKKコード
        public void DeleteTableSkkcode(string skkcode)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                string sqlstr;
                if (skkcode.Length >= 10)
                {
                    sqlstr = "DELETE FROM LC4Data WHERE SKKコード= '" + skkcode + "'";
                }
                else
                {
                    sqlstr = "DELETE FROM LC4Data WHERE SKKコード LIKE '" + skkcode + "%'";
                }
                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //新規LC4データの登録
        public void RegisterNewLC4Data(string skkcode, string data)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string dtstr = dt.ToString("yyyyMMddHHmm");
            try
            {
                DateTime dt2000 = new DateTime(2000, 1, 1, 0, 0, 0);
                TimeSpan ts = dt - dt2000;
                int count = (int)ts.TotalDays;
                string sqlstr = "INSERT INTO  LC4Data (ID,SKKコード,日付,データ) VALUES ('" + count + "','" + skkcode + "','" + dtstr + "','" + data + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


        }
    }
}