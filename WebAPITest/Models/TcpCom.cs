﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Sockets;

namespace WebAPITest.Models
{
    public class TcpCom
    { 
        //TCP通信用
        protected byte[] tcpSndBuf;
        protected byte[] tcpRcvBuf;
        protected TcpListener TcpServer;
        protected const int PortNumber = 10001;
        protected const int tcpSndBufSize = 1024;
        protected const int tcpRcvBufSize = 1024;
        public string tcpStr;
        public string idcode;
        public bool IsBusy;

        //constructor
        public TcpCom()
        {
            tcpSndBuf = new byte[tcpSndBufSize];
            tcpRcvBuf = new byte[tcpRcvBufSize];
            IsBusy = true;
            tcpStr = "";
        }

        //実行モジュール　指定SKKコード(idcode)に対して集信を実施
        public void TcpCom_Main()
        {
            try
            {
                TcpClient tcp;
                MRouterTable mbt = new MRouterTable();
                mbt.OpenTableSkkcode(idcode);
                if (mbt.GetNumOfRecord() == 0)
                {
                    IsBusy = false;
                    return;
                }
                string ipadr = mbt.GetIPAdr(0);
                int portno = mbt.GetPortNo(0);
                tcp = new System.Net.Sockets.TcpClient(ipadr, portno);
                tcp.SendTimeout = 500;
                tcp.ReceiveTimeout = 20000;
                NetworkStream stream = tcp.GetStream();
                string sndstr = "getlevedata1";
                byte[] sndbyte = System.Text.Encoding.ASCII.GetBytes(sndstr);
                stream.Write(sndbyte, 0, sndstr.Length);

                int readbyte;

                while (true)
                {
                    readbyte = stream.Read(tcpRcvBuf, 0, tcpRcvBufSize);
                    if (readbyte > 0)
                        break;
                }
                stream.Close();
                tcpStr += System.Text.Encoding.ASCII.GetString(tcpRcvBuf);
                tcp.Close();
                mbt.SetZaikoBuffer(0, tcpStr);
                mbt.UpdateDB();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}