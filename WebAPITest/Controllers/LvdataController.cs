﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace WebAPITest.Controllers
{
    //レべビジョンデータのREST APIを定義するコントローラクラス
    public class LvdataController : ApiController
    {
        const int ZSAVEINTERVAL = 6; //在庫保存間隔

        //[HttpGet("{id}")] Get api/Lvdata/?skkcode="SKKCODE"
        public string Get(string skkcode)
        {
            string data = "";
            if (skkcode.Length == 10)
            {
                string scode = skkcode.Substring(0, 10);
                MRouterTable mrt = new MRouterTable();
                mrt.OpenTableSkkcode(scode);
                data = mrt.GetZaikoString();
                if( data.Length > 10 )
                    data = "20" + data.Substring(0,10) + skkcode + data.Substring(10);

            } else if( (skkcode.Length == 12) && (skkcode.Substring(0,2) == "RT") )
            {
                string scode = skkcode.Substring(2, 10);
                MRouterTable mrt = new MRouterTable();
                mrt.OpenTableSkkcode(scode);
                if (mrt.GetNumOfRecord() == 1)
                {
                    TcpCom cthread = new TcpCom();
                    cthread.idcode = scode;
                    Thread CThread = new Thread((new ThreadStart(cthread.TcpCom_Main)));
                    CThread.IsBackground = true;
                    CThread.Start();
                    try
                    {
                        while (true)
                        {
                            if (cthread.IsBusy == false)
                                break;
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    DateTime dt = DateTime.Now.ToLocalTime();
                    string rcvtxt = cthread.tcpStr;
                    rcvtxt = rcvtxt.TrimEnd('\0');
                    if (rcvtxt.Length > 12)
                    {
                        rcvtxt = rcvtxt.Substring(12);
                        mrt.SetZaikoBuffer(0, rcvtxt);
                        mrt.UpdateDB();
                        data = dt.ToString("yyyyMMddHHmm") + scode + rcvtxt;
                    }
                }
            }
            return data;
        }

        // POST api/Lvdata/?skkcode="SKKCODE"data
        //[HttpPost]
        public void Post([FromBody]string value)
        {
            Console.Write(value);
            if (value.Length > 10)
            {
                string skkcode = value.Substring(0, 10);
                string zstring = value.Substring(10);
                //LogData lgd = new LogData();
                //DateTime dt = DateTime.Now.ToLocalTime();
                //lgd.OpenLogfileOfDay(dt);
                if (zstring.Length > 1000)
                    return;
                try
                {
                    MRouterTable mrt = new MRouterTable();
                    mrt.OpenTableSkkcode(skkcode);

                    if (mrt.GetNumOfRecord() == 0)
                    {
                        //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "No record");
                        //lgd.CloseLogfileOfDay(dt);
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Conflict));
                    }
                    mrt.SetZaikoBuffer(0, zstring);
                    if (mrt.UpdateDB() == false)
                    {
                        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                    }

                    SaveZaikoHistory(zstring, skkcode); //-> Webjobへ移動

                }
                catch (Exception ex)
                {
                    //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "Fail to open record");
                    //lgd.CloseLogfileOfDay(dt);
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return;
        }

        //在庫の保存
        private bool SaveZaikoHistory(string zdata, string skkcode)
        {
            try
            {
#if false
                //string zdata = cpt.GetZaikoAfterBuf(lineno); //在庫データの取得
                string timedt = "20" + zdata.Substring(0, 10);
                string zval;
                int[] ZaikoArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク在庫量
                int[] WtrlvlArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク水位
                int[] WtrvolArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク水量

                //6時間おきチェック　そうでなければリターン
                //string shour = timedt.Substring(8, 2);
                //string smin = timedt.Substring(10, 2);
                //int ihour = int.Parse(shour);
                //int imin = int.Parse(smin);
                //if ((ihour % ZSAVEINTERVAL) > 0)
                //    return false;
                //if (imin >= 10)
                //    return false;


                ZaikoStr zs = new ZaikoStr(zdata);
                zs.Analyze();
                int iTankNum = zs.numtank;
                for (int i = 0; i < iTankNum; i++)
                {
                    zval = zs.vollst[i]; //各タンクの在庫量
                    ZaikoArray[i] = int.Parse(zval) * 100;
                    if( zs.bwtrinf == true )
                    {
                        zval = zs.wtrlvllst[i]; //各タンクの水位
                        WtrlvlArray[i] = int.Parse(zval);
                        zval = zs.wtrvollst[i]; //各タンクの水量
                        WtrvolArray[i] = int.Parse(zval);
                    }
                    //iTankNum++;
                }
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;

                //各タンク在庫量をDBに登録
                sqlstr = "INSERT INTO InvHistory (ID, SKKコード,日付";
                for (int i = 1; i <= iTankNum; i++)
                {
                    sqlstr += ",タンク" + i.ToString() + "在庫量";
                }
                sqlstr = sqlstr + ") VALUES ('" + count + "','" + skkcode + "','" + timedt + "'";
                for (int i = 0; i < iTankNum; i++)
                {
                    sqlstr = sqlstr + ",'" + ZaikoArray[i].ToString() + "'";
                }
                sqlstr = sqlstr + ")";
                DBCtrl.ExecNonQuery(sqlstr);

                if (zs.bwtrinf == true)
                {
                    //各タンク在庫量をDBに登録
                    sqlstr = "INSERT INTO WtrHistory (ID, SKKコード,日付";
                    for (int i = 1; i <= iTankNum; i++)
                    {
                        sqlstr += ",タンク" + i.ToString() + "水位,タンク" + i.ToString() + "水量";
                    }
                    sqlstr = sqlstr + ") VALUES ('" + count + "','" + skkcode + "','" + timedt + "'";
                    for (int i = 0; i < iTankNum; i++)
                    {
                        sqlstr = sqlstr + ",'" + WtrlvlArray[i].ToString() + "','" + WtrvolArray[i].ToString() + "'";
                    }
                    sqlstr = sqlstr + ")";
                    DBCtrl.ExecNonQuery(sqlstr);
                }
#endif
                DateTime dt = DateTime.Now.ToLocalTime();
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                string timedt = "20" + zdata.Substring(0, 10);
                string sqlstr = "INSERT INTO InvData (ID, SKKコード,日付,在庫データ) VALUES ('" + count + "','" + skkcode + "','" + timedt + "','" + zdata + "')";
                DBCtrl.ExecNonQuery(sqlstr);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("在庫履歴書き込み失敗");
                return false;
            }
        }

    }


}
