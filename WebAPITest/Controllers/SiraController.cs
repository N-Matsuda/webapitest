﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;

namespace WebAPITest.Controllers
{
    //SIRAデータのREST APIを定義するコントローラクラス
    public class SiraController : ApiController
    {

        public string Get(string skkcode)
        {
            SiraCompany sc = new SiraCompany();
            sc.OpenTable("*");

            SiraSite sirass = new SiraSite();
            sirass.OpenTableWSkkcode(skkcode);
            string data = "";
            if (sirass.GetNumOfRecord() > 0)
            {
                SiraData sd = new SiraData();
                sd.OpenTableSkkcode(skkcode);
                if (sd.GetNumOfRecord() > 0)
                {
                    data = sd.GetSIRAData(0);
                    sd.DeleteTableSkkcode(skkcode);
                }
            }
            return data;
        }
        // POST api/Sira/?skkcode="SKKCODE"data
        //[HttpPost]
        public void Post([FromBody]string value)
        {
            Console.Write(value);
            if (value.Length > 10)
            {
                string skkcode = value.Substring(0, 10);
                string srstring = value.Substring(10);
                if (srstring.Length > 1024)
                    return;
                //LogData lgd = new LogData();
                //DateTime dt = DateTime.Now;
                //lgd.OpenLogfileOfDay(dt);
                try
                {
                    SiraSite sirass = new SiraSite();
                    sirass.OpenTableWSkkcode(skkcode);

                    if (sirass.GetNumOfRecord() == 1)
                    {
                        SiraData sd = new SiraData();
                        sd.RegisterNewSiraData(skkcode, srstring);
                        //SiraDataTrf sdt = new SiraDataTrf(skkcode);
                        //sdt.AnalyzeDailySIRA(srstring);
                    }
                }
                catch (Exception ex)
                {
                    //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "Fail to open record");
                    //lgd.CloseLogfileOfDay(dt);
                    //throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return;
        }


    }
}
