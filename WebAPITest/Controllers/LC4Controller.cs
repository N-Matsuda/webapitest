﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;

namespace WebAPITest.Controllers
{
    //LC4データのREST APIを定義するコントローラクラス
    public class LC4Controller : ApiController
    {
        public string Get(string skkcode)
        {
            string data = "LC4";
            return data;
        }

        // POST api/LC4/?skkcode="SKKCODE"data
        //[HttpPost]
        public void Post([FromBody]string value)
        {
            Console.Write(value);
            if (value.Length > 10)
            {
                string skkcode = value.Substring(0, 10);
                string lc4string = value.Substring(10);
                if (lc4string.Length > 2048)
                    return;
                try
                {
                    MRouterTable mrt = new MRouterTable();
                    mrt.OpenTableSkkcode(skkcode);
                    if (mrt.GetNumOfRecord() == 1)
                    {
                        LC4Data l4dat = new LC4Data();
                        l4dat.RegisterNewLC4Data(skkcode, lc4string);
                    }
                }
                catch (Exception ex)
                {
                    //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "Fail to open record");
                    //lgd.CloseLogfileOfDay(dt);
                    //throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return;
        }

    }
}
