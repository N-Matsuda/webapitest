﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;

namespace WebAPITest.Controllers
{
    public class SiraSalesController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }
        // POST api/SiraSales/?skkcode="SKKCODE"data
        // POST api/<controller>
        public void Post([FromBody]string value)
        {
            Console.Write(value);
            if (value.Length > 10)
            {
                string skkcode = value.Substring(0, 10);
                string srstring = value.Substring(10);
                if (srstring.Length > 1024)
                    return;
                //LogData lgd = new LogData();
                //DateTime dt = DateTime.Now;
                //lgd.OpenLogfileOfDay(dt);
                try
                {
                    SiraSite sirass = new SiraSite();
                    sirass.OpenTableWSkkcode(skkcode);

                    if (sirass.GetNumOfRecord() == 1)
                    {
                        SiraSalesData.RegisterNewSiraSalesData(skkcode, srstring);
                        //SiraDataTrf sdt = new SiraDataTrf(skkcode);
                        //sdt.AnalyzeDailySIRA(srstring);
                    }
                }
                catch (Exception ex)
                {
                    //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "Fail to open record");
                    //lgd.CloseLogfileOfDay(dt);
                    //throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
            return;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            return;
        }
    }
}