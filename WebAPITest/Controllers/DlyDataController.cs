﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;

namespace WebAPITest.Controllers
{
    //荷下ろしデータのREST APIを定義するコントローラクラス
    public class DlyDataController : ApiController
    {
        public string Get(string skkcode)
        {
            string data = "";
            return data;
        }

        // POST api/DlyData/?skkcode="SKKCODE"data
        //[HttpPost]
        public void Post([FromBody]string value)
        {
            Console.Write(value);
            if (value.Length > 10)
            {
                string skkcode = value.Substring(0, 10);
                string dlvstring = value.Substring(10);
                if (dlvstring.Length > 1500)
                    return;
                //LogData lgd = new LogData();
                //DateTime dt = DateTime.Now;
                //lgd.OpenLogfileOfDay(dt);
                try
                {
                    MRouterTable mrt = new MRouterTable();
                    mrt.OpenTableSkkcode(skkcode);
                    if (mrt.GetNumOfRecord() == 1)
                    {
                        DeliveryData lvdata = new DeliveryData();
                        lvdata.RegisterNewDeliveryData(skkcode, dlvstring);
                    }
                }
                catch (Exception ex)
                {
                    //lgd.WriteLog(dt.ToString("yyyymmdd"), skkcode, "Fail to open record");
                    //lgd.CloseLogfileOfDay(dt);
                    //throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.ExpectationFailed));
                }
            }
            return;
        }


    }
}
