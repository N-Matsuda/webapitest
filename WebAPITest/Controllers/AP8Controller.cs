﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;

namespace WebAPITest.Controllers
{
    //AP8データのREST APIを定義するコントローラクラス
    public class AP8Controller : ApiController
    {
        // GET api/AP8/values
        public string Get(string skkcode)
        {
            string data = "";
            if(skkcode == "cardlock")
            {
                FilePath fp = new FilePath();
                data += fp.DownloadCardLockFile();
            }
            return data;
        }

        // POST api/AP8/values
        // AP-8集信           =SKKコードGetAP8Datastatus(3digit)data....
        // AP-8再集信         =SKKコードRegetAP8Dtstatus(3digit)data....
        // Cardlockデータ     =SKKコードPostCLDatastatus(3digit)
        public void Post([FromBody]string value)
        {
            if (value.Length < 23)
                return;
            string skkcode = value.Substring(0, 10);
            SynaSiteDat ssd = new SynaSiteDat();
            ssd.OpenTable(skkcode);
            if (ssd.GetNumOfRecord() == 1)                      //該当SKKコードの登録があるならば
            {
                SynaSiteAP8Log synlog = new SynaSiteAP8Log();   //DBにAP-8実行結果保存
                synlog.InsertRecord(value);
                FilePath fp = new FilePath();
                fp.WriteTodayAP8Data(value.Substring(0, 10), value.Substring(23));  //blobにAP-8集信データを書き込む
                Console.Write(value);
            }
            return;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }


    }
}
